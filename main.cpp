#include <stdio.h>
#include "./lib/init.hpp"
#include <typeinfo>
#include <iostream>

class Prueba : public Object {
	public:
		const char *__name__ () {
			return this->obj_name (this);
		}
};

int main (int argc, char *argv []) {
	Logger.set_mode (true);
	Logger.set_color (Logger.red);
	// printf ("Hola mundo!\n");
	// Object obj;
	// printf ("%s\n", obj.__name__ ());
	// printf ("%s\n", obj.__repr__ ());
	// // printf ("%lli\n", Integer::max ()->c ());
	// // printf ("%lli\n", Integer::min ()->c ());
	// Integer num (750);
	// printf ("%s\n", num.__repr__ ());
	// ++ num;
	// printf ("%s\n", num.__repr__ ());
	// num ++;
	// printf ("%s\n", num.__repr__ ());
	// -- num;
	// printf ("%s\n", num.__repr__ ());
	// num --;
	// printf ("%s\n", num.__repr__ ());
	
	// printf ("%s\n", num.__name__ ());
	// char mini = 5;
	// printf ("short int: %hhi\n", mini);
	// printf ("%s\n", Logger.__repr__ ());
	// Logger.log ("Antes del primer string");
	// String nombre ("Diego");
	// printf ("Empieza por 'F'?: %s\n", nombre.startswith ("F")->__repr__ ());
	// printf ("Acaba por 'gor'?: %s\n", nombre.endswith ("gor")->__repr__ ());

	// printf ("'A' en binario: %s\n", String::char_to_string_binary ('A').__repr__ ());
	String *arabigo = new String ("asdfg");
	String *chino = new String ("用戶名格到");
	String *coreano = new String ("비밀번호하");
	String *ruso = new String ("парой");
	String *emojis = new String ("👀🎁🤢🎅👵");
	
	String *mixto = new String ("п비用d🤢");
	// {2, 3, 3, 1, 4}

	// printf ("True.__name__ (): %s\n", True->__name__ ());
	// printf ("arabigo.__name__ (): %s\n", arabigo->__name__ ());
	// printf ("None.__name__ (): %s\n", None->__name__ ());
	
	// printf ("%s\n", String::char_to_const_char_binary ('A'));
	// printf ("%s\n", String::char_to_string_binary ('A').__repr__ ());
	// printf ("Variables creadas.\n");
	printf ("arabigo: %s\n", arabigo->__repr__ ());
	printf ("chino: %s\n", chino->__repr__ ());
	printf ("coreano: %s\n", coreano->__repr__ ());
	printf ("ruso: %s\n", ruso->__repr__ ());
	printf ("emojis: %s\n", emojis->__repr__ ());
	printf ("mixto: %s\n", mixto->__repr__ ());
	printf ("Longitud en arabigo (5 simbolos): %lli\n", arabigo->length ()->c ());
	printf ("Longitud en chino (5 simbolos): %lli\n", chino->length ()->c ());
	printf ("Longitud en coreano (5 simbolos): %lli\n", coreano->length ()->c ());
	printf ("Longitud en ruso (5 simbolos): %lli\n", ruso->length ()->c ());
	printf ("Longitud en emojis (5 simbolos): %lli\n", emojis->length ()->c ());
	printf ("Longitud en mixto (5 simbolos): %lli\n", mixto->length ()->c ());

	printf ("Tamaño de bytes en arabigo (5 simbolos) (Se espera 5?): %lli\n", arabigo->size ()->c ());
	printf ("Tamaño de bytes en chino (5 simbolos) (Se espera 15?): %lli\n", chino->size ()->c ());
	printf ("Tamaño de bytes en coreano (5 simbolos) (Se espera 15?): %lli\n", coreano->size ()->c ());
	printf ("Tamaño de bytes en ruso (5 simbolos) (Se espera 10?): %lli\n", ruso->size ()->c ());
	printf ("Tamaño de bytes en emojis (5 simbolos) (Se espera 20?): %lli\n", emojis->size ()->c ());
	printf ("Tamaño de bytes en mixto (5 simbolos) (Se espera 13?): %lli\n", mixto->size ()->c ());

	// printf ("Del string 'mixto', saco el 9no caracter [8]: %c\n", mixto->cget (8));
	// printf ("Del string 'mixto', saco el 4to caracter [3]: %s\n", mixto->get (3));
	// String *prueba = new String ("s名번р");
	// printf ("Longitud de 'prueba' (4 simbolos): %s\n", prueba->length ()->__repr__ ());
	// printf ("A\n");

	// String *prueba2 = new String ("😊名áaa aá번акйр💕");
	
	// printf ("prueba2: %s\n", prueba2->__repr__ ());
	// printf ("prueba2 [9] (Se espera а (En ruso)): %s\n", (*prueba2) [9]->c ());
	// printf ("prueba2 [-3] (Se espera й (En ruso)): %s\n", (*prueba2) [-3]->c ());

	// printf ("Ruso:\n");
	
	// foreach (letra, ruso) {
	// 	printf ("%s\n", letra->__repr__ ());
	// }
	
	// String *concat = new String (mixto->__repr__ ());
	// (*concat) += " Jeje";
	// printf ("Concat: %s\n", concat->__repr__ ());
	// (*concat) = (*concat) * new Integer (5);
	// printf ("Concat * 5: %s\n", concat->__repr__ ());
	// printf ("Mixto: %s\n", mixto->__repr__ ());
	// printf ("Longitud en concat (50 simbolos): %lli\n", concat->length ()->c ());
	// printf ("Tamaño de bytes en concat (50 simbolos) (Se espera 90?): %lli\n", concat->size ()->c ());

	// String *lower = new String ("asdf");
	// printf ("%s.is_lower (): %s\n", lower->__repr__ (), lower->is_lower ()->__repr__ ());
	// (*lower) = (*lower).upper ();
	// printf ("%s.is_upper (): %s\n", lower->__repr__ (), lower->is_upper ()->__repr__ ());
	// (*lower) = (*lower).lower ();
	// (*lower) = (*lower).capitalize ();
	// printf ("%s.capitalize (): %s\n", lower->__repr__ (), lower->__repr__ ());
	
	// (*lower) = (*lower).swapcase ();
	// printf ("%s.swapcase (): %s\n", lower->__repr__ (), lower->__repr__ ());
	
	// Prueba *prueba3 = new Prueba ();
	// printf ("%s\n", prueba3->__repr__ ());
	// printf ("%s\n", prueba3->__name__ ());

	// String *prepend = lower->prepend (new String ("Prepend "));
	// printf ("%s\n", prepend->__repr__ ());
	// prepend->_prepend (new String ("Prepended "));
	// printf ("%s\n", prepend->__repr__ ());
	// String *digits = new String ("1234");

	// print ({new String ("arabigo.is_ascii () (Se espera True):"), arabigo->is_ascii ()});
	// print ({new String ("coreano.is_ascii () (Se espera False):"), coreano->is_ascii ()});
	// print ({new String ("mixto.is_ascii () (Se espera False):"), mixto->is_ascii ()});
	// print ({new String ("mixto.is_digit () (Se espera False):"), mixto->is_digit ()});
	// print ({new String ("digits.is_digit () (Se espera True):"), digits->is_digit ()});

	// String *clear = new String ("Clear");
	// print ({new String ("clear:"), clear});
	// clear->clear ();
	// print ({new String ("clear:"), clear});

	// String *asdf = new String ("AsDf");
	// print ({new String ("AsDf.equalsignorecase ('aSdF') (Se espera True):"), asdf->equalsignorecase ("aSdF")});
	// print ({new String ("AsDf.equalsignorecase ('aSdFg') (Se espera False):"), asdf->equalsignorecase ("aSdFg")});

	// String *joined = (new String ("-"))->join (prueba2);
	// print ({new String ("joined:"), joined});
	// print ({new String ("prueba2.length ():"), prueba2->length ()});
	// print ({new String ("joined.length ():"), joined->length ()});

	// String *to_lstrip = new String ("    lst rip");
	// String *to_rstrip = new String ("    rst rip     ");
	// print ({new String ("to_lstrip:"), to_lstrip});
	// (*to_lstrip) = to_lstrip->lstrip ();
	// print ({new String ("lstriped to_lstrip:"), to_lstrip});
	// print ({to_rstrip, new String ("end")}, NULL, new String (""));
	// (*to_rstrip) = to_rstrip->rstrip ();
	// print ({to_rstrip->rstrip (), new String ("end")}, NULL, new String (""));
	// print ({to_rstrip->strip ()}, new String ("end\n"));
	// try {
	// 	File *archivo = new File ("./file.txt", "a+");
	// 	archivo->append (new String ("Hola\n"));
	// 	archivo->close ();
	// 	print ({archivo});
	// 	print ({archivo->name ()});
	// } catch (int error) {
	// 	print ({new Integer (error)});
	// }

	std::cout << new Integer (456) << std::endl;
	std::cout << True << std::endl;
	std::cout << None << std::endl;
	std::cout << mixto << std::endl;


	return 0;
}
