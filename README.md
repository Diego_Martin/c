# Estructura de datos

## Descripcion :information_source:
El objetivo del programa es hacer una estructura de datos similar a la de Java, Python, Ruby o una combinación de las tres de manera que en futuros proyectos de C++ pueda manejar fácilmente los datos ('Fácilmente' se refiere a no tener que usar funciones de reserva dinámica de memoria como _malloc ()_, _calloc ()_, _realloc ()_ y _free ()_).

## Observaciones :eye:
 - Acerca de _String_:
    - He pensado en que sea tan solo un wrapper de _std::string_ que herede de _Object_, pero no es factible si quiero utilizar todos los tipos de codificación de caracteres, de hecho, _std::string_, aunque dicen que es "_utf-8_", en realidad es "_ascii_", por lo que no maneja bien las letras como la **ñ**, **á**, **é**, **í**, **ó**, **ú**, caracteres chinos, rusos, coreanos y no quiero imaginar los emoticonos...
	- ~~La solución al problema anterior es crear una clase _Character_, que sea un controlador dinámico de un puntero a uno o varios _char_, y que _String_ sea una lista dinámica de _Character_.~~
	- ¡El nuevo _String_ detecta cuánto ocupa cada caracter (1-4) y opera dependiendo de ello!
 - Hay que crear un sistema de logs personalizado. :white_check_mark:
 - Hay que crear todo el sistema de excepciones y errores.
 - Acerca de _Integer_:
   - Programar _Integer_ en binario para que no haya límite superior e inferior.
   - Debe ocupar el número de bytes necesario. ¡Nada de reservar toda la RAM!
 - Acerca de _Float_:
   - ~~Estará formado por 2 _Integer_.~~
   - Se programará en binario tambien

## Librerías :book:
 - **JInspired**
    - Para los amantes de Java, sería una de las librerías más extensas, porque dentro podría tener sublibrerías de Java. ArrayList, HashList, MultiSet, MultiMap, etc...
 - **Calendar**
    - Funciones del calendario
 - **DateTime**
    - Funciones para fechas (Necesario para crear _Calendar_)
 - **Math**
    - Funciones matemáticas
 - **MySql**
    - Conector de MySQL
 - **Os**
    - Funciones del sistema operativo
 - **Platform**
    - Funciones de detección y manejo del entorno
 - **Random**
    - Funciones relacionadas con la aleatoriedad
 - **Re**
    - Funciones de experesiones regulares
 - **Requests**
    - Funciones de peticiones HTTP
 - **Socket**
    - Funciones de protocolos en red (Necesario para crear _Requests_ y _WebServers_)
 - **Sys**
    - Funciones relacionadas con la propia librería
 - **Threading**
    - Funciones de programación concurrente
 - **Time**
    - Funciones relacionadas con el tiempo del sistema y temporalización del programa
 - **WebBrowser**
    - Funciones de control del navegador
 - **WebServers**
    - Creación de servidores web. Uno como imitacion de un servidor "de carpetas" (Como funciona Apache con PHP), otro como Django o Ruby on Rails, otro como Express (Con sus correspondientes opciones para programar con HBS o EJS etc) o Flask y por ultimo algo que funcione como React


¿Te imaginas?
