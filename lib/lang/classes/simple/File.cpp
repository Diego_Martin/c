#ifndef __LANG_CLASSES_SIMPLE_FILE_CPP__
#define __LANG_CLASSES_SIMPLE_FILE_CPP__

#include "File.hpp"

File::File (const char *route = NULL, const char *mode = "r") {
	if (route != NULL) {
		this->_name = (char *) malloc (sizeof (char) * (strlen (route) + 1));
		strcpy (this->_name, route);
		this->_mode = (char *) malloc (sizeof (char) * (strlen (mode) + 1));
		strcpy (this->_mode, mode);
		this->value = fopen (this->_name, this->_mode);
		this->is_opened = true;
	} else {
		this->value = stdout;
		this->_name = (char *) malloc (sizeof (char) * (strlen ("stdout") + 1));
		strcpy (this->_name, "stdout");
		this->_mode = (char *) malloc (sizeof (char) * (strlen (mode) + 1));
		strcpy (this->_mode, mode);
		this->value = fopen (this->_name, this->_mode);
		this->is_opened = true;
	}
}

File::File (FILE *value, const char *name, const char *mode = "r") {
	this->value = value;
	this->_name = (char *) malloc (sizeof (char) * (strlen (name) + 1));
	this->_mode = (char *) malloc (sizeof (char) * (strlen (mode) + 1));
	strcpy (this->_name, name);
	strcpy (this->_mode, mode);
	this->is_opened = true;
}

File::~File () {
	free (this->_name);
	free (this->_mode);
}

const char *File::__name__ () {
	return this->obj_name (this);
}

FILE *File::c () {
	return this->value;
}

String *File::name () {
	return new String (this->_name);
}

void File::append (Object *text) {
	if (this->is_opened)
		if (strcmp (this->_mode, "a") == 0 || strcmp (this->_mode, "a+") == 0)
			fprintf (this->value, "%s", text->__repr__ ());
		// else if (strcmp (this->_mode, "w") == 0 || strcmp (this->_mode, "w+") == 0)
		// 	fprintf (this->value, "%s%s", )
	else
		throw 3;
}

void File::close () {
	if (this->is_opened) {
		fclose (this->value);
		this->is_opened = false;
	} else {
		throw 3;
	}
}

#endif
