#ifndef __LANG_CLASSES_SIMPLE_NONETYPE_CPP__
#define __LANG_CLASSES_SIMPLE_NONETYPE_CPP__

#include "./NoneType.hpp"

NoneType::NoneType () {
	NoneType::count ++;

	this->value = NULL;

	if (NoneType::count > 1) {
		this->~NoneType ();
	}
}

NoneType::~NoneType () {
	free (this->value);
}

// Heredado de Object
const char *NoneType::__name__ () {
	return this->obj_name (this);
}

const char *NoneType::__repr__ () {
	return "None";
}

// Heredado de C
void *NoneType::c () {
	return this->value;
}

int NoneType::count = 0;

NoneType *None = new NoneType ();

#endif
