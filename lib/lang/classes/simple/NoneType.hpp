#ifndef __LANG_CLASSES_SIMPLE_NONETYPE_HPP__
#define __LANG_CLASSES_SIMPLE_NONETYPE_HPP__

#include "../../interfaces/Object.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <sstream>

class NoneType : public Object,
				 public C <void *> {
	public:
		NoneType ();
		
		~NoneType ();

		// Heredado de Object
		const char *__name__ ();

		const char *__repr__ ();

		// Heredado de C
		void *c ();

		static int count;

	private:
		void *value;
};

#endif
