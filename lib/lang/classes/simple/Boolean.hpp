#ifndef __LANG_CLASSES_SIMPLE_BOOLEAN_HPP__
#define __LANG_CLASSES_SIMPLE_BOOLEAN_HPP__


#include "../../interfaces/Object.hpp"
#include "../../interfaces/C.hpp"
#include "../../interfaces/Comparable.hpp"
#include "../../interfaces/Assignable.hpp"

class Boolean : public Object, 
				public C <bool>, 
				public Comparable <Boolean *>,
				public Comparable <Boolean>, 
				public Comparable <bool>,
				public Assignable <bool>, 
				public Assignable <Boolean>,
				public Assignable <Boolean *> {
	public:
		Boolean (bool value);
		Boolean (int value);

		// Heredado C <bool>
		bool c ();

		// Heredados de Object
		const char *__repr__ ();

		const char *__name__ ();

		// Heredado de Comparable <Boolean *>
		bool operator == (Boolean *value);

		// Heredado de Comparable <Boolean>
		bool operator == (Boolean value);

		// Heredado de Comparable <bool>
		bool operator == (bool value);

		// Heredado de Assignable <bool>
		void operator = (bool value);

		// Heredado de Assignable <Boolean *>
		void operator = (Boolean *value);

		// Heredado de Assignable <Boolean>
		void operator = (Boolean value);

		Boolean *operator ! ();

		Boolean *operator && (Boolean *value);

		Boolean *operator && (Boolean value);

		Boolean *operator && (bool value);

		Boolean *operator || (Boolean *value);

		Boolean *operator || (Boolean value);

		Boolean *operator || (bool value);

	private:
		bool value;
};

#endif