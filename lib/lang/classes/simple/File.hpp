#ifndef __LANG_CLASSES_SIMPLE_FILE_HPP__
#define __LANG_CLASSES_SIMPLE_FILE_HPP__

#include "../../interfaces/Object.hpp"
#include "../../interfaces/C.hpp"
#include "../../interfaces/Closeable.hpp"
#include "./String.cpp"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

class File : public Object, 
			 public C <FILE *>,
			 public Closeable {
	public:

		File (const char *route, const char *mode);

		File (FILE *value, const char *name, const char *mode);

		~File ();

		const char *__name__ ();

		FILE *c ();

		String *name ();

		void append (Object *text);

		void close ();
	
	private:
		FILE *value;
		char *_name;
		char *_mode;
		bool is_opened;
};

#endif
