#ifndef __LANG_CLASSES_SIMPLE_BOOLEAN_CPP__
#define __LANG_CLASSES_SIMPLE_BOOLEAN_CPP__

#include "Boolean.hpp"

Boolean::Boolean (bool value = false) {
	this->value = value;
}

Boolean::Boolean (int value) {
	this->value = (bool) value;
}

// Heredado C <bool>
bool Boolean::c () {
	return this->value;
}

// Heredados de Object
const char *Boolean::__repr__ () {
	return (this->value) ? "True" : "False";
}

const char *Boolean::__name__ () {
	return this->obj_name (this);
}

// Heredado de Comparable <Boolean *>
bool Boolean::operator == (Boolean *value) {
	return this->value == value->c ();
}

// Heredado de Comparable <Boolean>
bool Boolean::operator == (Boolean value) {
	return this->value == value.c ();
}

// Heredado de Comparable <bool>
bool Boolean::operator == (bool value) {
	return this->value == value;
}

// Heredado de Assignable <bool>
void Boolean::operator = (bool value) {
	this->value = value;
}

// Heredado de Assignable <Boolean *>
void Boolean::operator = (Boolean *value) {
	this->value = value->c ();
}

// Heredado de Assignable <Boolean>
void Boolean::operator = (Boolean value) {
	this->value = value.c ();
}

Boolean *Boolean::operator ! () {
	return new Boolean (!(this->value));
}

Boolean *Boolean::operator && (Boolean *value) {
	return new Boolean (this->value && value->c ());
}

Boolean *Boolean::operator && (Boolean value) {
	return new Boolean (this->value && value.c ());
}

Boolean *Boolean::operator && (bool value) {
	return new Boolean (this->value && value);
}

Boolean *Boolean::operator || (Boolean *value) {
	return new Boolean (this->value || value->c ());
}

Boolean *Boolean::operator || (Boolean value) {
	return new Boolean (this->value || value.c ());
}

Boolean *Boolean::operator || (bool value) {
	return new Boolean (this->value || value);
}


Boolean *True = new Boolean (true);
Boolean *False = new Boolean (false);

#endif
