#ifndef __LANG_CLASSES_SIMPLE_FLOAT_HPP__
#define __LANG_CLASSES_SIMPLE_FLOAT_HPP__

#include "../../interfaces/Object.hpp"

#include <sstream>

class Float : public Object,
              public C <long double>,
              public Assignable <int>,
              public Assignable <float>,
              public Assignable <double>,
              public Assignable <long double>,
              public Assignable <Float>,
              public Assignable <Float *>,
              public Comparable <int>,
              public Comparable <float>,
              public Comparable <double>,
              public Comparable <long double>,
              public Comparable <Float>,
              public Comparable <Float *> {
    public:
        Float () {
            this->value = 0;
        }

        Float (int value) {
            this->value = (long double) value;
        }

        Float (float value) {
            this->value = value;
        }

        Float (double value) {
            this->value = value;
        }

        Float (long double value) {
            this->value = value;
        }

        // Heredado de Object
        const char *__name__ () {
            return "Float";
        }

        const char *__repr__ () {
            std::stringstream canal;
            canal << this->value;
            char *ret = (char *) malloc (sizeof (char) * canal.str ().size ());
            strcpy (ret, canal.str ().c_str ());
            return ret;
        }

        // Heredado de C
        long double c () {
            return this->value;
        }

        // Heredado de Assignable
        void operator = (int value) {
            this->value = value;
        }

        void operator = (float value) {
            this->value = value;
        }

        void operator = (double value) {
            this->value = value;
        }

        void operator = (long double value) {
            this->value = value;
        }

        void operator = (Float value) {
            this->value = value.c ();
        }

        void operator = (Float *value) {
            this->value = value->c ();
        }

        // Heredado de Comparable
        bool operator == (int value) {
            return this->value == value;
        }

        bool operator == (float value) {
            return this->value == value;
        }

        bool operator == (double value) {
            return this->value == value;
        }

        bool operator == (long double value) {
            return this->value == value;
        }

        bool operator == (Float value) {
            return this->value == value.c ();
        }

        bool operator == (Float *value) {
            return this->value == value->c ();
        }
    private:
        long double value;
};

#endif
