#ifndef __LANG_CLASSES_SIMPLE_INTEGER_HPP__
#define __LANG_CLASSES_SIMPLE_INTEGER_HPP__


#include "../../interfaces/Object.hpp"
#include "../../interfaces/C.hpp"
#include "../../interfaces/Assignable.hpp"
#include "../../interfaces/Comparable.hpp"
#include "./Float.hpp"

#include <limits.h>
#include <sstream>

/*
 * Hacer dinamico en binario
 */


class Integer : public Object, 
				public C <long long int>//,
				// public Assignable <int>, 
				// public Assignable <long int>, 
				// public Assignable <long long int>, 
				// public Assignable <unsigned int>,
				// public Assignable <unsigned long int>, 
				// public Assignable <unsigned long long int>,
				// public Assignable <Integer>,
				// public Assignable <Integer *>, 
				// public Comparable <int>, 
				// public Comparable <long int>, 
				// public Comparable <long long int>, 
				// public Comparable <unsigned int>, 
				// public Comparable <unsigned long int>, 
				// public Comparable <unsigned long long int>, 
				// public Comparable <Integer>, 
				// public Comparable <Integer *>,
				// public Comparable <float>,
				// public Comparable <double>,
				// public Comparable <long double>,
				// public Comparable <Float>,
				// public Comparable <Float *>
				{
	public:
		Integer () {
			this->value = 0;
		}

		Integer (int value) {
			this->value = value;
		}

		Integer (long int value) {
			this->value = value;
		}

		Integer (long long int value) {
			this->value = value;
		}

		Integer (unsigned int value) {
			this->value = value;
		}

		Integer (unsigned long int value) {
			this->value = value;
		}

		Integer (unsigned long long int value) {
			this->value = value;
		}

		// Heredados de object
		const char *__name__ () {
			return this->obj_name (this);
		}

		const char *__repr__ () {
			std::stringstream canal;
			canal << this->value;
			return canal.str ().c_str ();
		}

		// Heredados de C <long long int>
		long long int c () {
			return this->value;
		}

		// // Heredados de Asignable
		// void operator = (int value) {
		// 	this->value = value;
		// }
		
		// void operator = (long int value) {
		// 	this->value = value;
		// }
		
		// void operator = (long long int value) {
		// 	this->value = value;
		// }
		
		// void operator = (unsigned int value) {
		// 	this->value = value;
		// }
		
		// void operator = (unsigned long int value) {
		// 	this->value = value;
		// }
		
		// void operator = (unsigned long long int value) {
		// 	this->value = value;
		// }

		// void operator = (Integer value) {
		// 	this->value = value.c ();
		// }

		// void operator = (Integer *value) {
		// 	this->value = value->c ();
		// }

		// // Heredados de Comparable
		// bool operator == (int value) {
		// 	return this->value == value;
		// }

		// bool operator == (long int value) {
		// 	return this->value == value;
		// }

		// bool operator == (long long int value) {
		// 	return this->value == value;
		// }

		// bool operator == (unsigned int value) {
		// 	return this->value == value;
		// }

		// bool operator == (unsigned long int value) {
		// 	return this->value == value;
		// }

		// bool operator == (unsigned long long int value) {
		// 	return this->value == value;
		// }

		// bool operator == (Integer value) {
		// 	return this->value == value.c ();
		// }

		// bool operator == (Integer *value) {
		// 	return this->value == value->c ();
		// }

		// bool operator == (float value) {
		// 	return this->value == value;
		// }

		// bool operator == (double value) {
		// 	return this->value == value;
		// }

		// bool operator == (long double value) {
		// 	return this->value == value;
		// }

		// bool operator == (Float value) {
		// 	return this->value == value.c ();
		// }

		// bool operator == (Float *value) {
		// 	return this->value == value->c ();
		// }


		// // Heredados de Comparable
		// bool operator < (int value) {
		// 	return this->value < value;
		// }

		// bool operator < (long int value) {
		// 	return this->value < value;
		// }

		// bool operator < (long long int value) {
		// 	return this->value < value;
		// }

		// bool operator < (unsigned int value) {
		// 	return this->value < value;
		// }

		// bool operator < (unsigned long int value) {
		// 	return this->value < value;
		// }

		// bool operator < (unsigned long long int value) {
		// 	return this->value < value;
		// }

		// bool operator < (Integer value) {
		// 	return this->value < value.c ();
		// }

		// bool operator < (Integer *value) {
		// 	return this->value < value->c ();
		// }
		
		// bool operator < (float value) {
		// 	return this->value < value;
		// }

		// bool operator < (double value) {
		// 	return this->value < value;
		// }

		// bool operator < (long double value) {
		// 	return this->value < value;
		// }

		// bool operator < (Float value) {
		// 	return this->value < value.c ();
		// }

		// bool operator < (Float *value) {
		// 	return this->value < value->c ();
		// }


		// // Heredados de Comparable
		// bool operator > (int value) {
		// 	return this->value > value;
		// }

		// bool operator > (long int value) {
		// 	return this->value > value;
		// }

		// bool operator > (long long int value) {
		// 	return this->value > value;
		// }

		// bool operator > (unsigned int value) {
		// 	return this->value > value;
		// }

		// bool operator > (unsigned long int value) {
		// 	return this->value > value;
		// }

		// bool operator > (unsigned long long int value) {
		// 	return this->value > value;
		// }

		// bool operator > (Integer value) {
		// 	return this->value > value.c ();
		// }

		// bool operator > (Integer *value) {
		// 	return this->value > value->c ();
		// }
		
		// bool operator > (float value) {
		// 	return this->value > value;
		// }

		// bool operator > (double value) {
		// 	return this->value > value;
		// }

		// bool operator > (long double value) {
		// 	return this->value > value;
		// }

		// bool operator > (Float value) {
		// 	return this->value > value.c ();
		// }

		// bool operator > (Float *value) {
		// 	return this->value > value->c ();
		// }

		// // Heredados de Comparable
		// bool operator <= (int value) {
		// 	return this->value <= value;
		// }

		// bool operator <= (long int value) {
		// 	return this->value <= value;
		// }

		// bool operator <= (long long int value) {
		// 	return this->value <= value;
		// }

		// bool operator <= (unsigned int value) {
		// 	return this->value <= value;
		// }

		// bool operator <= (unsigned long int value) {
		// 	return this->value <= value;
		// }

		// bool operator <= (unsigned long long int value) {
		// 	return this->value <= value;
		// }

		// bool operator <= (Integer value) {
		// 	return this->value <= value.c ();
		// }

		// bool operator <= (Integer *value) {
		// 	return this->value <= value->c ();
		// }
		
		// bool operator <= (float value) {
		// 	return this->value <= value;
		// }

		// bool operator <= (double value) {
		// 	return this->value <= value;
		// }

		// bool operator <= (long double value) {
		// 	return this->value <= value;
		// }

		// bool operator <= (Float value) {
		// 	return this->value <= value.c ();
		// }

		// bool operator <= (Float *value) {
		// 	return this->value <= value->c ();
		// }

		// // Heredados de Comparable
		// bool operator >= (int value) {
		// 	return this->value >= value;
		// }

		// bool operator >= (long int value) {
		// 	return this->value >= value;
		// }

		// bool operator >= (long long int value) {
		// 	return this->value >= value;
		// }

		// bool operator >= (unsigned int value) {
		// 	return this->value >= value;
		// }

		// bool operator >= (unsigned long int value) {
		// 	return this->value >= value;
		// }

		// bool operator >= (unsigned long long int value) {
		// 	return this->value >= value;
		// }

		// bool operator >= (Integer value) {
		// 	return this->value >= value.c ();
		// }

		// bool operator >= (Integer *value) {
		// 	return this->value >= value->c ();
		// }
		
		// bool operator >= (float value) {
		// 	return this->value >= value;
		// }

		// bool operator >= (double value) {
		// 	return this->value >= value;
		// }

		// bool operator >= (long double value) {
		// 	return this->value >= value;
		// }

		// bool operator >= (Float value) {
		// 	return this->value >= value.c ();
		// }

		// bool operator >= (Float *value) {
		// 	return this->value >= value->c ();
		// }

		// // Heredados de Comparable
		// long long int operator + (int value) {
		// 	return this->value + value;
		// }

		// long long int operator + (long int value) {
		// 	return this->value + value;
		// }

		// long long int operator + (long long int value) {
		// 	return this->value + value;
		// }

		// long long int operator + (unsigned int value) {
		// 	return this->value + value;
		// }

		// long long int operator + (unsigned long int value) {
		// 	return this->value + value;
		// }

		// long long int operator + (unsigned long long int value) {
		// 	return this->value + value;
		// }

		// long long int operator + (Integer value) {
		// 	return this->value + value.c ();
		// }

		// long long int operator + (Integer *value) {
		// 	return this->value + value->c ();
		// }
		
		// long long int operator + (float value) {
		// 	return (long long int) (this->value + value);
		// }

		// long long int operator + (double value) {
		// 	return (long long int) (this->value + value);
		// }

		// long long int operator + (long double value) {
		// 	return (long long int) (this->value + value);
		// }

		// long long int operator + (Float value) {
		// 	return (long long int) (this->value + value.c ());
		// }

		// long long int operator + (Float *value) {
		// 	return (long long int) (this->value + value->c ());
		// }

		// // Heredados de Comparable
		// long long int operator - (int value) {
		// 	return this->value - value;
		// }

		// long long int operator - (long int value) {
		// 	return this->value - value;
		// }

		// long long int operator - (long long int value) {
		// 	return this->value - value;
		// }

		// long long int operator - (unsigned int value) {
		// 	return this->value - value;
		// }

		// long long int operator - (unsigned long int value) {
		// 	return this->value - value;
		// }

		// long long int operator - (unsigned long long int value) {
		// 	return this->value - value;
		// }

		// long long int operator - (Integer value) {
		// 	return this->value - value.c ();
		// }

		// long long int operator - (Integer *value) {
		// 	return this->value - value->c ();
		// }
		
		// long long int operator - (float value) {
		// 	return (long long int) (this->value - value);
		// }

		// long long int operator - (double value) {
		// 	return (long long int) (this->value - value);
		// }

		// long long int operator - (long double value) {
		// 	return (long long int) (this->value - value);
		// }

		// long long int operator - (Float value) {
		// 	return (long long int) (this->value - value.c ());
		// }

		// long long int operator - (Float *value) {
		// 	return (long long int) (this->value - value->c ());
		// }

		// // Heredados de Comparable
		// long long int operator * (int value) {
		// 	return this->value * value;
		// }

		// long long int operator * (long int value) {
		// 	return this->value * value;
		// }

		// long long int operator * (long long int value) {
		// 	return this->value * value;
		// }

		// long long int operator * (unsigned int value) {
		// 	return this->value * value;
		// }

		// long long int operator * (unsigned long int value) {
		// 	return this->value * value;
		// }

		// long long int operator * (unsigned long long int value) {
		// 	return this->value * value;
		// }

		// long long int operator * (Integer value) {
		// 	return this->value * value.c ();
		// }

		// long long int operator * (Integer *value) {
		// 	return this->value * value->c ();
		// }

		
		// long long int operator * (float value) {
		// 	return (long long int) (this->value * value);
		// }

		// long long int operator * (double value) {
		// 	return (long long int) (this->value * value);
		// }

		// long long int operator * (long double value) {
		// 	return (long long int) (this->value * value);
		// }

		// long long int operator * (Float value) {
		// 	return (long long int) (this->value * value.c ());
		// }

		// long long int operator * (Float *value) {
		// 	return (long long int) (this->value * value->c ());
		// }

		// // Heredados de Comparable
		// long double operator / (int value) {
		// 	return (long double) this->value / value;
		// }

		// long double operator / (long int value) {
		// 	return (long double) this->value / value;
		// }

		// long double operator / (long long int value) {
		// 	return (long double) this->value / value;
		// }

		// long double operator / (unsigned int value) {
		// 	return (long double) this->value / value;
		// }

		// long double operator / (unsigned long int value) {
		// 	return (long double) this->value / value;
		// }

		// long double operator / (unsigned long long int value) {
		// 	return (long double) this->value / value;
		// }

		// long double operator / (Integer value) {
		// 	return (long double) this->value / value.c ();
		// }

		// long double operator / (Integer *value) {
		// 	return (long double) this->value / value->c ();
		// }

		
		// long double operator / (float value) {
		// 	return (long double) (this->value / value);
		// }

		// long double operator / (double value) {
		// 	return (long double) (this->value / value);
		// }

		// long double operator / (long double value) {
		// 	return (long double) (this->value / value);
		// }

		// long double operator / (Float value) {
		// 	return (long double) (this->value / value.c ());
		// }

		// long double operator / (Float *value) {
		// 	return (long double) (this->value / value->c ());
		// }

		// // Heredados de Comparable
		// long long int operator % (int value) {
		// 	return this->value % value;
		// }

		// long long int operator % (long int value) {
		// 	return this->value % value;
		// }

		// long long int operator % (long long int value) {
		// 	return this->value % value;
		// }

		// long long int operator % (unsigned int value) {
		// 	return this->value % value;
		// }

		// long long int operator % (unsigned long int value) {
		// 	return this->value % value;
		// }

		// long long int operator % (unsigned long long int value) {
		// 	return this->value % value;
		// }

		// long long int operator % (Integer value) {
		// 	return this->value % value.c ();
		// }

		// long long int operator % (Integer *value) {
		// 	return this->value % value->c ();
		// }



		// // Heredados de Comparable
		// void operator += (int value) {
		// 	this->value += value;
		// }

		// void operator += (long int value) {
		// 	this->value += value;
		// }

		// void operator += (long long int value) {
		// 	this->value += value;
		// }

		// void operator += (unsigned int value) {
		// 	this->value += value;
		// }

		// void operator += (unsigned long int value) {
		// 	this->value += value;
		// }

		// void operator += (unsigned long long int value) {
		// 	this->value += value;
		// }

		// void operator += (Integer value) {
		// 	this->value += value.c ();
		// }

		// void operator += (Integer *value) {
		// 	this->value += value->c ();
		// }
		
		// void operator += (float value) {
		// 	this->value += (long long int) value;
		// }

		// void operator += (double value) {
		// 	this->value += (long long int) value;
		// }

		// void operator += (long double value) {
		// 	this->value += (long long int) value;
		// }

		// void operator += (Float value) {
		// 	this->value += (long long int) value.c ();
		// }

		// void operator += (Float *value) {
		// 	this->value += (long long int) value->c ();
		// }

		// // Heredados de Comparable
		// void operator -= (int value) {
		// 	this->value -= value;
		// }

		// void operator -= (long int value) {
		// 	this->value -= value;
		// }

		// void operator -= (long long int value) {
		// 	this->value -= value;
		// }

		// void operator -= (unsigned int value) {
		// 	this->value -= value;
		// }

		// void operator -= (unsigned long int value) {
		// 	this->value -= value;
		// }

		// void operator -= (unsigned long long int value) {
		// 	this->value -= value;
		// }

		// void operator -= (Integer value) {
		// 	this->value -= value.c ();
		// }

		// void operator -= (Integer *value) {
		// 	this->value -= value->c ();
		// }
		
		// void operator -= (float value) {
		// 	this->value -= (long long int) value;
		// }

		// void operator -= (double value) {
		// 	this->value -= (long long int) value;
		// }

		// void operator -= (long double value) {
		// 	this->value -= (long long int) value;
		// }

		// void operator -= (Float value) {
		// 	this->value -= (long long int) value.c ();
		// }

		// void operator -= (Float *value) {
		// 	this->value -= (long long int) value->c ();
		// }

		// // Heredados de Comparable
		// void operator *= (int value) {
		// 	this->value *= value;
		// }

		// void operator *= (long int value) {
		// 	this->value *= value;
		// }

		// void operator *= (long long int value) {
		// 	this->value *= value;
		// }

		// void operator *= (unsigned int value) {
		// 	this->value *= value;
		// }

		// void operator *= (unsigned long int value) {
		// 	this->value *= value;
		// }

		// void operator *= (unsigned long long int value) {
		// 	this->value *= value;
		// }

		// void operator *= (Integer value) {
		// 	this->value *= value.c ();
		// }

		// void operator *= (Integer *value) {
		// 	this->value *= value->c ();
		// }
		
		// void operator *= (float value) {
		// 	this->value *= (long long int) value;
		// }

		// void operator *= (double value) {
		// 	this->value *= (long long int) value;
		// }

		// void operator *= (long double value) {
		// 	this->value *= (long long int) value;
		// }

		// void operator *= (Float value) {
		// 	this->value *= (long long int) value.c ();
		// }

		// void operator *= (Float *value) {
		// 	this->value *= (long long int) value->c ();
		// }

		// // Heredados de Comparable
		// void operator /= (int value) {
		// 	this->value /= value;
		// }

		// void operator /= (long int value) {
		// 	this->value /= value;
		// }

		// void operator /= (long long int value) {
		// 	this->value /= value;
		// }

		// void operator /= (unsigned int value) {
		// 	this->value /= value;
		// }

		// void operator /= (unsigned long int value) {
		// 	this->value /= value;
		// }

		// void operator /= (unsigned long long int value) {
		// 	this->value /= value;
		// }

		// void operator /= (Integer value) {
		// 	this->value /= value.c ();
		// }

		// void operator /= (Integer *value) {
		// 	this->value /= value->c ();
		// }

		// // Heredados de Comparable
		// void operator %= (int value) {
		// 	this->value %= value;
		// }

		// void operator %= (long int value) {
		// 	this->value %= value;
		// }

		// void operator %= (long long int value) {
		// 	this->value %= value;
		// }

		// void operator %= (unsigned int value) {
		// 	this->value %= value;
		// }

		// void operator %= (unsigned long int value) {
		// 	this->value %= value;
		// }

		// void operator %= (unsigned long long int value) {
		// 	this->value %= value;
		// }

		// void operator %= (Integer value) {
		// 	this->value %= value.c ();
		// }

		// void operator %= (Integer *value) {
		// 	this->value %= value->c ();
		// }
		
		// // Postfix increment
		// Integer& operator ++ (int value = 0) {
		// 	if (value == 0) {
		// 		this->value ++;
		// 	} else {
		// 		this->value += value;
		// 	}
		// 	return *this;
		// }
		
		// // Prefix increment
		// Integer& operator ++ () {
		// 	this->value ++;
		// 	return *this;
		// }

		// // Postfix decrement
		// Integer& operator -- (int value = 0) {
		// 	if (value == 0) {
		// 		this->value --;
		// 	} else {
		// 		this->value += value;
		// 	}
		// 	return *this;
		// }
		
		// // Prefix decrement
		// Integer& operator -- () {
		// 	this->value --;
		// 	return *this;
		// }

		// // Propios
		// static long long int cmax () {
		// 	return LLONG_MAX;
		// }
		
		// static long long int cmin () {
		// 	return LLONG_MIN;
		// }

		// static Integer *max () {
		// 	return new Integer (Integer::cmax ());
		// }
		
		// static Integer *min () {
		// 	return new Integer (Integer::cmin ());
		// }
	private:
		long long int value;
};

#endif
