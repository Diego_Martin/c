#ifndef __LANG_CLASSES_SIMPLE_INTEGER_HPP__
#define __LANG_CLASSES_SIMPLE_INTEGER_HPP__


#include "../../interfaces/Object.hpp"
#include "../../interfaces/C.hpp"
#include "../../interfaces/Assignable.hpp"
#include "../../interfaces/Comparable.hpp"
#include "./Float.hpp"

#include <limits.h>
#include <string>

class Integer : public Object, 
				public C <long long int> {
	public:
		// Constructores
		Integer (unsigned long long int value = 0);

		Integer (std::string &value);

		Integer (const char *value);

		Integer (Integer &value);

		// Heredado de Object
		const char *__repr__ ();

		const char *__name__ ();

		// Heredado de C <long long int>
		long long int c ();

		// Helpers
		void divide_by_2 ();

		bool Null ();

		int Length ();

		int operator [] (const int) const;

		bool operator == (const Integer &value);

		bool operator != (const Integer &value);

		bool operator < (const Integer &value);

		bool operator > (const Integer &value);

		bool operator <= (const Integer &value);

		bool operator >= (const Integer &value);

		Integer &operator += (const Integer &value);

		Integer operator + (const Integer &value);

	private:
		std::string value;
	
};

#endif
