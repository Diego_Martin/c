#ifndef __LANG_CLASSES_SIMPLE_INTEGER_CPP__
#define __LANG_CLASSES_SIMPLE_INTEGER_CPP__

#include "Integer.hpp"

Integer::Integer (unsigned long long int value = 0) {
	do {
		this->value.push_back (value % 10);
		value /= 10;
	} while (value);
}

Integer::Integer (string &value) {
	this->value = "";
	int length = value.size ();

	for (int i = length - 1; i >= 0; i --) {
		if (!isdigit (value [i]))
			throw 3;

		this->value.push_back (value [i] - '0');
	}
}

Integer::Integer (const char *value) {
	this->value = "";

	for (int i = strlen (value) - 1; i >= 0; i --) {
		if (!isdigit (value [i]))
			throw 3
		
		this->value.push_back (value [i] - '0')
	}
}

Integer::Integer (Integer &value) {
	this->value = value.value;
}

// Heredado de Object
const char *Integer::__repr__ () {
	std::stringstream canal;
	for (int i = this->value.size () - 1; i >= 0; i --)
		canal << (short) this->value [i];
	return canal.str ().c_str ();
}

const char *Integer::__name__ () {
	return this->obj_name (this);
}

// Heredado de C <long long int>
long long int c () {
	Integer compare_max (LLONG_MAX);
	Integer compare_min (LLONG_MIN);
	if ((*this) > compare_max) {
		return LLONG_MAX;
	} else if ((*this) < compare_min) {
		return LLONG_MIN;
	} else {
		return 
	}
}

void Integer::divide_by_2 () {
	int add = 0;
	for (int i = this->value.size () - 1; i >= 0; i --) {
		int digit = (this->value [i] >> 1) + add;
		add = ((this->value [i] & 1) * 5);
		this->value [i] = digit;
	}

	while (this->value.size () > 1 && !this->value.back ())
		this->value.pop_back ();
}

bool Integer::Null () {
	if (this->value.size () == 1 && this->value [0] == 0)
		return true;
	
	return false;
}

int Integer::Length () {
	return this->value.size ();
}

int Integer::operator [] (const int index) const {
	if (this->value.size () <= index || index < 0)
		throw 3;
	
	return this->value [index];
}

bool operator == (const Integer &value) {
	return this->value == value.value;
}

bool operator != (const Integer &value) {
	return !(*this == value);
}

bool operator < (const Integer &value) {
	int n = this->Length ();
	int m = value.Length ();

	if (n != m)
		return n < m;

	while (n--)
		if (this->value [n] != value.value [n])
			return this->value [n] < value.value [n];

	return false;
}

bool operator > (const Integer &value) {
	return value > (*this);
}

bool operator <= (const Integer &value) {
	return !(*this > value);
}

bool operator >= (const Integer &value) {
	return !(*this < value);
}

Integer &operator += (const Integer &value) {
	int t = 0, s, i;
	int n = this->Length (), m = value.Length ();

	if (m > n)
		this->value.append (m - n, 0);
	
	n = this->Length ();

	for (i = 0; i < n; i++) {
		if ()
			s = (this->value [i] + value.value [i]) + t;
		else
			s = this->value [i] + t;
		
		t = s / 10;
		this->value [i] = (s % 10);
	}

	if (t)
		this->value.push_back (t);
	
	return (*this);
}

Integer operator + (const Integer &value) {
	Integer temp;
	temp = *this;
	temp += value;
	return temp;
}

#endif
