#ifndef __LANG_CLASSES_SIMPLE_STRING_HPP__
#define __LANG_CLASSES_SIMPLE_STRING_HPP__

#include "../../interfaces/Assignable.hpp"
#include "../../interfaces/C.hpp"
#include "../../interfaces/Comparable.hpp"
#include "../../interfaces/Iterable.hpp"
#include "../../interfaces/Object.hpp"
#include "./Integer.hpp"

#include <math.h>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// UTF-Lo que necesites

// Crear String a partir de char, char *, const char *, int, (vacios), y cualquier objeto derivado de 'Object' ✅
// Portabilidad a C por const char * ✅
// Es iterable ✅
// Métodos
//  - void operator=(const char *) ✅
//  - void operator=(String) ✅
//  - void operator=(String *) ✅
//  - bool operator==(const char *) ✅
//  - bool operator==(String) ✅
//  - bool operator==(String *) ✅
//  - String *operator += (String value) ✅
//  - String *operator += (String *value) ✅
//  - String *operator += (const char *value) ✅
//  - String *operator += (char value) ✅
//  - String *operator += (int value) ✅
//  - String *operator + (const char *value) ✅
//  - String *operator + (String value) ✅
//  - String *operator + (Object *value) ✅
//  - String *operator * (int value) ✅
//  - String *operator * (Integer *value) ✅
//  - String *operator [] (int index) ✅
//  - String *operator [] (Integer index) ✅
//  - String *operator [] (Integer *index) ✅
//  - char cget (int index) ✅
//  - Integer *length () ✅
//  - Integer *size () ✅
//  - String *lower () ✅
//  - void _lower () ✅
//  - String *upper () ✅
//  - void _upper () ✅
//  - String *capitalize () ✅
//  - void _capitalize () ✅
//  - String *swapcase () ✅
//  - void _swapcase () ✅
//  - Boolean *is_lower () ✅
//  - Boolean *is_upper () ✅
//  - Boolean *is_ascii () ✅
//  - Boolean *is_digit () ✅
//  - Boolean *is_integer () ✅
//  - Boolean *is_float ()
//  - void clear () // Vacia el string ✅
//  - String *strip (String *character = new String (" ")) ✅ // Llama a this->lstrip (character) y a this->rstrip (character)
//  - String *lstrip (String *character = new String (" ")) ✅
//  - String *rstrip (String *character = new String (" ")) ✅
//  - Integer *count (String *value) // Cuenta cuántas veces aparece 'value' en el String
//  - String *replaceall (String *replaced, String *to_replace)
//  - String *replacefirst (String *replaced, String *to_replace, int occurrences = 1)
//  - String *replacefirst (String *replaced, String *to_replace, Integer *occurrences = new Integer (1))
//  - String *replacelast (String *replaced, String *to_replace, int occurrences = 1)
//  - String *replacelast (String *replaced, String *to_replace, Integer *occurrences = new Integer (1))
//  - String *translate (Dict *diccionary) // Reemplaza con clave-valor
//  - String *charfillstart (String *character, Integer *size) // Siendo character->length () == 1, rellena con 'character' por delante
//  - String *charfillend (String *character, Integer *size) // Siendo character->length () == 1, rellena con 'character' por detras
//  - String *zfill (Integer *size) // LLama a this->charfillstart (new String ("0"), size)
//  - Integer *findfirst (String *search) // Devuelve el indice de la primera aparicion de un string
//  - Integer *findlast (String *search) // Devuelve el indice de la última aparicion de un string
//  - List *findall (String *search) // Devuelve un array con todos los indices en los que aparece un subsring
//  - String *join (Iterable *collection) // Une la colección con el valor actual (this->value) como pegamento ✅
//  - List *split (String *separator) // Separa el string en una lista cortando por los separadores
//  - List *splitlines () // Llama a this->split (new String ("\n"));
//  - String *slice (Integer *start, Integer *end, Integer *step = new Integer (1)) // Substring con salto
//  - Boolean *equalsignorecase (String *) // Compara ignorando mayusculas y minusculas ✅
//  - Boolean *equalsignorecase (String) // Compara ignorando mayusculas y minusculas ✅
//  - Boolean *equalsignorecase (const char *) // Compara ignorando mayusculas y minusculas ✅
//  - String *prepend (String *) // Concatena strings con el parámetro primero ✅
//  - void _prepend (String *) // Concatena strings con el parámetro primero ✅
//  - Boolean *startswith (const char *value) ✅
//  - Boolean *endswith (const char *value) ✅
//  - Boolean *startswith (String *value) ✅
//  - Boolean *endswith (String *value) ✅
//  - static String char_to_string_binary (char character) ✅
//  - static const char *char_to_const_char_binary (char character) ✅

class String : public Object, 
			   public C <const char *>,
			   public Comparable <const char *>,
			   public Comparable <String *>, 
			   public Comparable <String>, 
			   public Iterable, 
			   public Assignable <const char *>,
			   public Assignable <String>, 
			   public Assignable <String *> {
	public:
		String ();

		String (char value);

		String (int value);

		String (char *value);

		String (const char *value);

		String (Object *value);

		~String ();

		// derivado de Object
		const char *__name__ ();

		const char *__repr__ ();

		const char *__str__ ();

		// heredado de C <const char *>
		const char *c ();

		// heredado de Comparable
		bool operator == (String *value);

		bool operator == (String value);

		bool operator == (const char *value);

		// heredado de Iterable
		Object *begin ();

		Object *end ();

		Object *next ();

		// Heredados de Assignable
		void operator = (String value);

		void operator = (String *value);

		void operator = (const char *value);

		String *operator += (String value);

		String *operator += (String *value);

		String *operator += (const char *value);

		String *operator += (char value);

		String *operator += (int value);

		String *operator + (const char *value);

		String *operator + (String value);

		String *operator + (Object *value);

		String *operator * (int value);

		String *operator * (Integer *value);

		String *operator [] (int index);

		String *operator [] (Integer index);
		
		String *operator [] (Integer *index);

		char cget (int index);

		// propios
		Integer *length ();
		
		Integer *size ();

		String *lower ();

		String *upper ();

		void _lower ();

		void _upper ();

		Boolean *is_lower ();

		Boolean *is_upper ();

		Boolean *is_ascii ();

		Boolean *is_digit ();

		Boolean *is_integer ();

		String *capitalize ();

		void _capitalize ();

		String *swapcase ();

		void _swapcase ();

		Boolean *startswith (const char *value);

		Boolean *endswith (const char *value);

		Boolean *startswith (String *value);

		Boolean *endswith (String *value);

		String *prepend (String *value);

		void _prepend (String *value);

		void clear ();

		Boolean *equalsignorecase (String *value);

		Boolean *equalsignorecase (String value);

		Boolean *equalsignorecase (const char *value);

		String *join (Iterable *collection);

		String *strip (String *character); // Llama a this->lstrip (character) y a this->rstrip (character)
		
		String *lstrip (String *character);
		
		String *rstrip (String *character);

		static String char_to_string_binary (char character) {
			String return_value ("");
			for (int i = 7; i >= 0; --i)
				return_value += (character & (1 << i)) ? '1' : '0';
			
			return return_value;
		}

		static const char *char_to_const_char_binary (char character) {
			char *return_value = (char *) malloc (sizeof (char) * 8);
			strcpy (return_value, "");
			for (int i = 7; i >= 0; --i)
				strcat (return_value, (character & (1 << i)) ? "1" : "0");
			
			return (const char *) return_value;
		}

	private:
		short int *char_size_map;
		char *value;
		String **iteration_array;
		long long int iterator;
		char *aux;


		void fill_char_size_map ();
		const char *get (int index);
};

#endif
