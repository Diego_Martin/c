#ifndef __LANG_CLASSES_SIMPLE_INTEGER_CPP__
#define __LANG_CLASSES_SIMPLE_INTEGER_CPP__

#include "Integer.hpp"

Integer::Integer (long long int value = 0) : negative (value < 0) {
	if (value < 0)
		value = -1 * value;

	do {
		this->value.push_back (value % 10);
		value /= 10;
	} while (value);
}

Integer::Integer (const std::string &value) : negative (value [0] == '-') {
	if (value [0] == '-') {
		for (int i = 0; i < value.size () - 1; i ++)
			value [i] = value [i + 1];

		value.pop_back ();
	}

	
}

long long int Integer::c () {
	// If this <= LLONG_MAX && this >= LLONG_MIN
	if (this <= LLONG_MAX && this >= LLONG_MIN) {
		long long int return_value = 0;
		for (int i = 0; i < this->value.size (); i ++) {
			int cifras = 1;
			for (int cifra = 0; cifra < i; cifra ++, cifras *= 10);
			
			return_value += (cifras * ((short) this->value [i]));
		}
		return return_value;
	} else {
		if (this > 0) {
			return LLONG_MAX;
		} else {
			return LLONG_MIN;
		}
	}
}

#endif
