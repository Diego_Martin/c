#ifndef __LANG_CLASSES_SIMPLE_INTEGER_HPP__
#define __LANG_CLASSES_SIMPLE_INTEGER_HPP__


#include "../../interfaces/Object.hpp"
#include "../../interfaces/C.hpp"
#include "../../interfaces/Assignable.hpp"
#include "../../interfaces/Comparable.hpp"
#include "./Float.hpp"

#include <limits.h>
#include <string>

class Integer : public Object, 
				public C <long long int> {
	public:
		// Constructores
		Integer (long long int value = 0);
		Integer (const std::string &value = 0);

		// Heredado de C <long long int>
		long long int c ();

	private:
		std::string value;
		bool negative;
	
};

#endif
