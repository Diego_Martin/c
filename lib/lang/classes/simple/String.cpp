#ifndef __LANG_CLASSES_SIMPLE_STRING_CPP__
#define __LANG_CLASSES_SIMPLE_STRING_CPP__

#include "./String.hpp"

String::String () {
	this->aux = (char *) calloc (0, sizeof (char));
	this->value = NULL;
	this->char_size_map = (short int *) calloc (0, sizeof (short int));
	this->fill_char_size_map ();
}

String::String (char value) {
	this->aux = (char *) calloc (0, sizeof (char));
	this->value = (char *) malloc (sizeof (char) * 2);
	sprintf (this->value, "%c", value);
	this->char_size_map = (short int *) calloc (0, sizeof (short int));
	this->fill_char_size_map ();
}

String::String (int value) {
	this->aux = (char *) calloc (0, sizeof (char));
	this->value = (char *) malloc (sizeof (char) * 2);
	sprintf (this->value, "%c", (char) value);
	this->char_size_map = (short int *) calloc (0, sizeof (short int));
	this->fill_char_size_map ();
}

String::String (char *value) {
	this->aux = (char *) calloc (0, sizeof (char));
	this->value = (char *) malloc (sizeof (char) * strlen (value));
	sprintf (this->value, "%s", value);
	this->char_size_map = (short int *) calloc (0, sizeof (short int));
	this->fill_char_size_map ();
}

String::String (const char *value) {
	this->aux = (char *) calloc (0, sizeof (char));
	this->value = (char *) malloc (sizeof (char) * strlen (value));
	sprintf (this->value, "%s", value);
	// Logger.log ("Valor fijado");
	this->char_size_map = (short int *) calloc (0, sizeof (short int));
	// Logger.log ("char_size_map reservado (0 bytes)");
	this->fill_char_size_map ();
}

String::String (Object *value) {
	this->aux = (char *) calloc (0, sizeof (char));
	this->value = (char *) malloc (sizeof (char) * strlen (value->__repr__ ()));
	sprintf (this->value, "%s", value->__repr__ ());
	this->char_size_map = (short int *) calloc (0, sizeof (short int));
	this->fill_char_size_map ();
}

String::~String () {
	free (this->value);
	free (this->aux);
	free (this->char_size_map);
	free (this->iteration_array);
}

// derivado de Object
const char *String::__name__ () {
	return this->obj_name (this);
}

const char *String::__repr__ () {
	return (const char *) this->value;
}

const char *String::__str__ () {
	this->aux = (char *) realloc (this->aux, sizeof (char) * (strlen (this->value) + 2));
	strcpy (this->aux, "'");
	strcat (this->aux, this->value);
	strcat (this->aux, "'");
	return (const char *) this->aux;
}

// heredado de C <const char *>
const char *String::c () {
	return this->__repr__ ();
}

// heredado de Comparable
bool String::operator == (String *value) {
	return strcmp (this->__repr__ (), value->__repr__ ()) == 0;
}

bool String::operator == (String value) {
	return (*this) == (&value);
}

bool String::operator == (const char *value) {
	return (*this) == new String (value);
}

// heredado de Iterable
Object *String::begin () {
	this->iterator = 0;
	this->iteration_array = (String **) realloc (this->iteration_array, sizeof (String *) * this->length ()->c ());
	for (int i = 0; i < this->length ()->c (); i++)
		*(this->iteration_array + i) = new String (this->get (i));

	return this->iteration_array [0];
}

Object *String::end () {
	return this->iteration_array [this->length ()->c ()];
	// return new String ("a");
}

Object *String::next () {
	this->iterator++;
	return this->iteration_array [this->iterator];
	// return new String ("a");
}

// Heredados de Assignable
void String::operator = (String value) {
	this->value = (char *) realloc (this->value, sizeof (char) * strlen (value.__repr__ ()));
	strcpy (this->value, value.__repr__ ());
	this->fill_char_size_map ();
}

void String::operator = (String *value) {
	this->value = (char *) realloc (this->value, sizeof (char) * strlen (value->__repr__ ()));
	strcpy (this->value, value->__repr__ ());
	this->fill_char_size_map ();
}

void String::operator = (const char *value) {
	this->value = (char *) realloc (this->value, sizeof (char) * strlen (value));
	strcpy (this->value, value);
	this->fill_char_size_map ();
}

String *String::operator += (String value) {
	std::stringstream canal;
	canal << this->value << value.__repr__ ();
	*this = canal.str ().c_str ();
	this->fill_char_size_map ();
	return this;
}

String *String::operator += (String *value) {
	std::stringstream canal;
	canal << this->value << value->__repr__ ();
	this->value = (char *) realloc (this->value, sizeof (char) * (strlen (value->__repr__ ()) + strlen (this->value)));
	strcpy (this->value, canal.str ().c_str ());
	this->fill_char_size_map ();
	return this;
}

String *String::operator += (const char *value) {
	std::stringstream canal;
	canal << this->value << value;
	this->value = (char *) realloc (this->value, sizeof (char) * (strlen (value) + strlen (this->value)));
	strcpy (this->value, canal.str ().c_str ());
	this->fill_char_size_map ();
	return this;
}

String *String::operator += (char value) {
	std::stringstream canal;
	canal << this->value << value;
	this->value = (char *) realloc (this->value, sizeof (char) * (1 + strlen (this->value)));
	strcpy (this->value, canal.str ().c_str ());
	this->fill_char_size_map ();
	return this;
}

String *String::operator += (int value) {
	std::stringstream canal;
	canal << this->value << value;
	this->value = (char *) realloc (this->value, sizeof (char) * (strlen (canal.str ().c_str ())));
	strcpy (this->value, canal.str ().c_str ());
	this->fill_char_size_map ();
	return this;
}

String *String::operator + (const char *value) {
	std::stringstream canal;
	canal << this->value << value;
	this->fill_char_size_map ();
	return new String (canal.str ().c_str ());
}

String *String::operator + (String value) {
	std::stringstream canal;
	canal << this->value << value.__repr__ ();
	return new String (canal.str ().c_str ());
}

String *String::operator + (Object *value) {
	std::stringstream canal;
	canal << this->value << value->__repr__ ();
	return new String (canal.str ().c_str ());
}

String *String::operator * (int value) {
	if (value < 1)
		return new String ("");
	
	if (value == 1)
		return new String (this->value);
	
	std::stringstream canal;
	for (int i = 0; i < value; i++)
		canal << this->__repr__ ();
		
	return new String (canal.str ().c_str ());
}

String *String::operator * (Integer *value) {
	return (*this) * (int)(value->c ());
}

String *String::operator [] (int index) {
	return new String (this->get (index));
}

String *String::operator [] (Integer index) {
	return new String (this->get ((int) index.c ()));
}

String *String::operator [] (Integer *index) {
	return new String (this->get ((int) index->c ()));
}

char String::cget (int index) {
	return *(this->value + index);
}

// propios
Integer *String::length () {
	int return_value = 0;
	
	for (int i = 0; i < this->size ()->c (); i ++) {
		const char *binary = String::char_to_const_char_binary (*(this->value + i));

		if (binary != strstr (binary, "10"))
			return_value ++;
	}
	return new Integer (return_value);
}

Integer *String::size () {
	return new Integer (strlen (this->value));
}

String *String::lower () {
	this->aux = (char *) realloc (this->aux, sizeof (char) * strlen (this->c ()));
	strcpy (this->aux, this->c ());
	
	for (int i = 0; i < strlen (this->aux); i ++)
		if (isupper (*(this->aux + i)))
			*(this->aux + i) = tolower (*(this->aux + i));

	return new String (this->aux);
}

String *String::upper () {
	this->aux = (char *) realloc (this->aux, sizeof (char) * strlen (this->c ()));
	strcpy (this->aux, this->c ());
	
	for (int i = 0; i < strlen (this->aux); i ++)
		if (islower (*(this->aux + i)))
			*(this->aux + i) = toupper (*(this->aux + i));

	return new String (this->aux);
}

void String::_lower () {
	for (int i = 0; i < strlen (this->value); i ++)
		if (isupper (*(this->value + i)))
			*(this->value + i) = tolower (*(this->value + i));

}

void String::_upper () {
	for (int i = 0; i < strlen (this->value); i ++)
		if (islower (*(this->value + i)))
			*(this->value + i) = toupper (*(this->value + i));
}

Boolean *String::is_lower () {
	bool return_value = true;
	for (int i = 0; i < strlen (this->value); i ++)
		if (!islower (*(this->value + i)))
			return_value = false;

	return (return_value) ? True : False;
}

Boolean *String::is_upper () {
	bool return_value = true;
	for (int i = 0; i < strlen (this->value); i ++)
		if (!isupper (*(this->value + i)))
			return_value = false;

	return (return_value) ? True : False;
}

Boolean *String::is_ascii () {
	bool return_value = true;
	this->fill_char_size_map ();

	for (int i = 0; i < sizeof (this->char_size_map) / sizeof (short int); i ++)
		if (this->char_size_map [i] != 1)
			return_value = false;

	return (return_value) ? True : False;
}

Boolean *String::is_digit () {
	bool return_value = true && this->is_ascii ()->c ();

	if (!return_value)
		return False;

	for (int i = 0; i < strlen (this->value); i ++)
		if (!isdigit (*(this->value + i)))
			return_value = false;
	
	return (return_value) ? True : False;
}

Boolean *String::is_integer () {
	return this->is_digit ();
}

String *String::capitalize () {
	this->aux = (char *) realloc (this->aux, sizeof (char) * strlen (this->value));
	strcpy (this->aux, this->value);
	if (islower (*(this->aux)))
		*(this->aux) = toupper (*(this->aux));

	return new String (this->aux);
}

void String::_capitalize () {
	if (islower (*(this->value)))
		*(this->value) = toupper (*(this->value));
}

String *String::swapcase () {
	this->aux = (char *) realloc (this->aux, sizeof (char) * strlen (this->value));
	strcpy (this->aux, this->value);
	for (int i = 0; i < strlen (this->aux); i ++)
		if (islower (*(this->aux + i)))
			*(this->aux + i) = toupper (*(this->aux + i));
		else
			if (isupper (*(this->aux + i)))
				*(this->aux + i) = tolower (*(this->aux + i));

	return new String (this->aux);
}

void String::_swapcase () {
	for (int i = 0; i < strlen (this->value); i ++)
		if (islower (*(this->value + i)))
			*(this->value + i) = toupper (*(this->value + i));
		else
			if (isupper (*(this->value + i)))
				*(this->value + i) = tolower (*(this->value + i));
}

Boolean *String::startswith (const char *value) {
	return (this->value == strstr (this->value, value)) ? True : False;
}

Boolean *String::endswith (const char *value) {
	return ((this->value + (strlen (this->value) - strlen (value))) == strstr (this->value, value)) ? True : False;
}

Boolean *String::startswith (String *value) {
	return (this->value == strstr (this->value, value->c ())) ? True : False;
}

Boolean *String::endswith (String *value) {
	return ((this->value + (strlen (this->value) - strlen (value->c ()))) == strstr (this->value, value->c ())) ? True : False;
}

String *String::prepend (String *value) {
	std::stringstream canal;
	canal << value->__repr__ () << this->value;
	
	return new String (canal.str ().c_str ());
}

void String::_prepend (String *value) {
	std::stringstream canal;
	canal << value->__repr__ () << this->value;
	this->value = (char *) realloc (this->value, sizeof (char) * strlen (canal.str ().c_str ()));
	strcpy (this->value, canal.str ().c_str ());
}

void String::clear () {
	free (this->value);
	this->value = (char *) calloc (0, sizeof (char));
	strcpy (this->value, "");
}

Boolean *String::equalsignorecase (String *value) {
	return (strcmp (this->lower ()->__repr__ (), value->lower ()->__repr__ ()) == 0) ? True : False;
}

Boolean *String::equalsignorecase (String value) {
	return this->equalsignorecase (&value);
}

Boolean *String::equalsignorecase (const char *value) {
	return this->equalsignorecase (new String (value));
}

void String::fill_char_size_map () {
	this->char_size_map = (short int *) realloc (this->char_size_map, sizeof (short int) * this->length ()->c ());

	int iterator = 0;
	for (int i = 0; i < this->size ()->c (); i ++) {
		const char *binary = String::char_to_const_char_binary (*(this->value + i));
		// Logger.log ("Binary: %s", binary);

		// Tiene 1 byte
		if (binary == strstr (binary, "0")) {
			this->char_size_map [iterator] = 1;
		// Tiene 2 bytes
		} else if (binary == strstr (binary, "110")) {
			this->char_size_map [iterator] = 2;
			continue;
		// Tiene 3 bytes
		} else if (binary == strstr (binary, "1110")) {
			this->char_size_map [iterator] = 3;
			i ++;
			continue;
		// Tiene 4 bytes
		} else if (binary == strstr (binary, "11110")) {
			this->char_size_map [iterator] = 4;
			i += 2;
			continue;
		}
		iterator ++;
	}
}

const char *String::get (int index) {
	// Obtiene el caracter por índice
	//
	// El retorno es de tipo (const char *) por si el caracter seleccionado tiene más de 1 byte (Ejemplos: "ó" (2 bytes), "밀" (3 bytes), "😊" (4 bytes))
	// Además de ésta manera se puede convertir fácilmente a 'String' en las funciones String::operator [](*)
	//
	// --- Es posible que ésta función se tenga que hacer privada ---
	//
	// Ejemplo:
	// Del string "Hóla" queremos obtener "l":
	// Se salta 1 byte de 'H'
	// Se salta 2 bytes de "ó"
	// Detecta que 'l' sólo tiene 1 byte y lo devuelve en un (const char *)
	//
	// Pasos:
	//     1.- Detecta si el índice es correcto (index >= (this->length () * -1) && index < this->length ()->c ()), si no, lanza una excepción.
	//	   2.- Si es (index < 0) hace un (this->get (this->length ()->c () + index) | this->get (this->length ()->c () - abs(index)))
	//     3.- Si es (index >= 0):
	//         1.- Rellena un array de enteros (int) con las longitudes de los caracteres (1, 2, 3 o 4 bytes) ("Hóla" sería {1, 2, 1, 1})
	//         2.- Selecciona cuál de los items de ésa lista quiere obtener. ("Hóla" [2] sería {1, 2, 1, 1} [2])
	//         3.- Suma y almacena en 'jump_length' los bytes que se tiene que saltar para llegar al caracter, almacena en 'return_length' cuántos bytes tiene el caracter seleccionado. ({1, 2, 1, 1} tiene (({1, 2, 1, 1} [0] + {1, 2, 1, 1} [1])(1 + 2) = 3) bytes para saltarse y {1, 2, 1, 1} [2] = 1 para almacenar)
	//         4.- Con la función 'strstr ()' obtiene lo que necesita
	//
	// ------ Adelante máquina tu puedes! ------
	// DONEEEE!!!!
	this->fill_char_size_map ();
	if (index > -1 && index < this->length ()->c ()) {
		short int jump_length = 0;
		short int return_length = this->char_size_map [index];

		for (int i = 0; i < index; i ++)
			jump_length += this->char_size_map [i];
		
		std::stringstream return_string;
		return_string.write (this->value + jump_length, return_length);

		return return_string.str ().c_str ();

	} else if (index < 0 && index >= (this->length ()->c () * -1)) {
		index = this->length ()->c () - abs (index);
		return this->get (index);

	} else {
		return "";

	}
}

String *String::join (Iterable *collection) {
	String *return_value = new String ("");
	int longitud = 0;
	int vuelta = 0;

	foreach (item, collection) {
		longitud ++;
	}

	foreach (item, collection) {
		(*return_value) += item->__repr__ ();
		if (vuelta < (longitud - 1))
			(*return_value) += this->value;
		
		vuelta ++;
	}

	return return_value;
}

String *String::strip (String *character = new String (" ")) {
	String *return_string = new String (this->value);
	return_string = return_string->lstrip (character);
	return_string = return_string->rstrip (character);
	return return_string;
}

String *String::lstrip (String *character = new String (" ")) {
	bool empieza = true;
	std::stringstream canal;
	foreach (item, this) {
		if (!((((String *) item)->operator == (character)) && empieza)) {
			canal << item->__repr__ ();
			empieza = false;
		}
	}
	
	return new String (canal.str ().c_str ());
}

String *String::rstrip (String *character = new String (" ")) {
	bool empieza = false;
	String *return_string = new String ("");

	for (int i = (int) this->length ()->c () - character->length ()->c () - 1; i >= 0; i --) {
		if (strcmp (this->get (i), character->__repr__ ()) != 0 || empieza) {
			return_string->_prepend (new String (this->get (i)));
			empieza = true;
		}
	}
	return return_string;
}

#endif
