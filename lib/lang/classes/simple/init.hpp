#ifndef __LANG_CLASSES_SIMPLE_INIT_HPP__
#define __LANG_CLASSES_SIMPLE_INIT_HPP__

#include "NoneType.cpp"
#include "Boolean.cpp"
#include "Integer.hpp"
#include "Float.hpp"
#include "File.cpp"
#include "String.cpp"

#endif
