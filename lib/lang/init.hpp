#ifndef __LANG_INIT_HPP__
#define __LANG_INIT_HPP__

#define foreach(item, iterable) for(Object *item = iterable->begin (); item != iterable->end (); item = iterable->next ())
#define unless(condition) if(!(condition))

#include "./interfaces/init.hpp"
#include "./classes/init.hpp"
#include "./builtins/init.hpp"

#endif
