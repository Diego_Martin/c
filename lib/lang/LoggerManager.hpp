#ifndef __LANG_LOGGERMANAGER_HPP__
#define __LANG_LOGGERMANAGER_HPP__

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "./interfaces/Object.hpp"

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
	//define something for Windows (32-bit and 64-bit, this part is common)
	#define SO "Windows"
	#define MiWindows
	#include <windows.h>
	#ifdef _WIN64
		//define something for Windows (64-bit only)
		#define ARQUITECTURA "x64"
	#else
		//define something for Windows (32-bit only)
		#define ARQUITECTURA "x32"
	#endif
#elif __APPLE__
	#include <TargetConditionals.h>
	#if TARGET_IPHONE_SIMULATOR
		// iOS Simulator
		#define SO "Simulador IOS"
		#define ARQUITECTURA ""
	#elif TARGET_OS_IPHONE
		// iOS device
		#define SO "IOS"
		#define ARQUITECTURA ""
	#elif TARGET_OS_MAC
		// Other kinds of Mac OS
		#define SO "Mac OS"
		#define ARQUITECTURA ""
	#else
		#define SO "Apple desconocido"
		#define ARQUITECTURA ""
	#endif
#elif __linux__
	#define MiLinux
	#include <unistd.h>
	#define SO "Linux"
	#define ARQUITECTURA ""
#elif __unix__ // all unices not caught above
	#define MiUnix
	#include <unistd.h>
	#define SO "Unix"
	#define ARQUITECTURA ""
#elif defined(_POSIX_VERSION)
	#define MiPosix
	#include <unistd.h>
	#define SO "Posix"
	#define ARQUITECTURA ""
#else
	#define SO "Compilador desconocido"
	#define ARQUITECTURA ""
#endif


class LoggerManager : public Object {
	public:
		#if defined (MiWindows)
			int reset = 7;

			int black = 0;
			int blue = 1;
			int green = 2;
			int cyan = 3;
			int red = 4;
			int magenta = 5;
			int yellow = 6;
			int white = 7;

			int blue_hi = 9;
			int green_hi = 10;
			int cyan_hi = 11;
			int red_hi = 12;
			int magenta_hi = 13;
			int yellow_hi = 14;
			int white_hi = 15;
			int black_hi = 16;

			int color;
		#elif defined MiLinux
			const char *reset = "\033[0m";

			const char *black = "\033[30;1m";
			const char *red = "\033[31;1m";
			const char *green = "\033[32;1m";
			const char *yellow = "\033[33;1m";
			const char *blue = "\033[34;1m";
			const char *magenta = "\033[35;1m";
			const char *cyan = "\033[36;1m";
			const char *white = "\033[37;1m";

			const char *black_hi = "\033[90;1m";
			const char *red_hi = "\033[91;1m";
			const char *green_hi = "\033[92;1m";
			const char *yellow_hi = "\033[93;1m";
			const char *blue_hi = "\033[94;1m";
			const char *magenta_hi = "\033[95;1m";
			const char *cyan_hi = "\033[96;1m";
			const char *white_hi = "\033[97;1m";
			
			const char *color;
		#endif

		bool verbose = false;

		#if defined (MiWindows)
			LoggerManager (bool verbose, int color = 2) {
		#elif defined MiLinux
			LoggerManager (bool verbose, const char *color = "\033[32;1m") {
		#endif
			this->color = color;
			this->verbose = verbose;
		}

		// Heredado de Object
		const char *__name__ () {
			return this->obj_name (this);
		}

		// const char *__repr__ () {
		// 	return typeid (this->color).name ();
		// }

		int log (const char *formato = "", ...) {
			if (this->verbose) {
				va_list argumentos;
				int exito = 1;
				#if defined (MiWindows)
					HANDLE consola;
					consola = GetStdHandle(STD_OUTPUT_HANDLE);
					SetConsoleTextAttribute (consola, this->color);
				#elif defined MiLinux
					printf ("%s", this->color);
				#endif
				va_start (argumentos, formato);
				exito = vfprintf (stdout, formato, argumentos);
				va_end (argumentos);
				#if defined (MiWindows)
					SetConsoleTextAttribute (consola, this->reset);
					printf ("\n");
				#elif defined MiLinux
					printf ("%s\n", this->reset);
				#endif

				return exito;
			} else {
				return 0;
			}
		}

		#if defined (MiWindows)
			void set_color (int color) {
		#elif defined MiLinux
			void set_color (const char *color) {
		#endif
			this->color = color;
		}

		void set_mode (bool value) {
			this->verbose = value;
		}
};

#endif