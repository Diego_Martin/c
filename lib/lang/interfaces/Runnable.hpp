#ifndef __RUNNABLE_HPP__
#define __RUNNABLE_HPP__

class Runnable {
	public:
		virtual void run () = 0;
};

#endif