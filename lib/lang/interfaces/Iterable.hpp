#ifndef __ITERABLE_HPP__
#define __ITERABLE_HPP__

#include "./Object.hpp"

// Son los objetos iterables por un foreach
class Iterable {
	public:
		virtual Object *begin () = 0;
		virtual Object *end () = 0;
		virtual Object *next () = 0;
};

#endif