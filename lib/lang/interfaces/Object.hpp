#ifndef __LANG_OBJECT_HPP__
#define __LANG_OBJECT_HPP__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <typeinfo>
#include <iostream>

class Object {
	public:
		virtual const char *__name__ () {
			// Devuelve el nombre de la clase
			return this->obj_name (this);
		}

		virtual const char *__repr__ () {
			char *res = (char *) malloc (sizeof (char) * (28 + strlen (this->__name__ ())));
			sprintf (res, "<class '%s' in %p>", this->__name__ (), this);
			return (const char *) res;
		}

		virtual const char *__str__ () {
			return this->__repr__ ();
		}
		
		template <class Type>
		const char *obj_name (Type *object) {
			// Devuelve el nombre de la clase
			// Normalmente, la expresión [[ typeid (this).name () ]] devolvería algo como [[ P6Object ]], por lo que hay que quitarle los 2 primeros caracteres
			char *return_string = (char *) malloc (sizeof (char) * (strlen (typeid (object).name ()) - 2));
			const char *original_name = typeid (object).name ();
			strncpy (return_string, &original_name [2], strlen (typeid (object).name ()) - 2);
			return (const char *) return_string;
		}

		friend std::ostream& operator << (std::ostream& canal, Object *object) {
			canal << object->__repr__ ();
		}
};

#endif