#ifndef __ASSIGNABLE_HPP__
#define __ASSIGNABLE_HPP__

template <class Type>
class Assignable {
	public:
		virtual void operator = (Type value) = 0;
};

#endif