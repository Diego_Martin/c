#ifndef __COMPARABLE_HPP__
#define __COMPARABLE_HPP__

template <class Type>
class Comparable {
	public:
		virtual bool operator == (Type value) = 0;
};

#endif