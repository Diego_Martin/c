#ifndef __LANG_INTERFACES_HPP__
#define __LANG_INTERFACES_HPP__

#include "./Object.hpp"
#include "./Assignable.hpp"
#include "./C.hpp"
#include "./Comparable.hpp"
#include "./Closeable.hpp"
#include "./Iterable.hpp"

#endif
