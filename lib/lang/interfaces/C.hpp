#ifndef __C_HPP__
#define __C_HPP__

// Interfaz plantilla para los tipos que deban representarse como primitivos
template <class Type>
class C {
	public:
		virtual Type c () = 0;
};

#endif