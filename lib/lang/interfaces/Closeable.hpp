#ifndef __CLOSEABLE_HPP__
#define __CLOSEABLE_HPP__

class Closeable {
	public:
		virtual void close () = 0;
};

#endif