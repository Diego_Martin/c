#ifndef __LANG_BUILTINS_PRINT_CPP__
#define __LANG_BUILTINS_PRINT_CPP__

#include "../interfaces/Object.hpp"
#include "../classes/simple/String.cpp"
#include "../classes/simple/File.cpp"
#include "../classes/simple/Boolean.cpp"

#include <stdio.h>
#include <stdlib.h>


NoneType *print (std::initializer_list <Object *> items, String *end = new String ("\n"), String *sep = new String (" "), File *file = new File (stdout, "stdout"), Boolean *flush = False) {
	if (end == NULL)
		end = new String ("\n");

	if (file == NULL)
		file = new File (stdout, "stdout");
	
	if (flush == NULL)
		flush = False;

	if (file->c () == stdin) {
		fprintf (stderr, "\033[31;40mERROR: No se puede imprimir 'Object' en el canal 'Sys::Stdin' (C: stdin) porque 'Sys::Stdin' es un canal de entrada de datos, no de salida.\033[97;40m\n\n");
		exit (1);
	}

	int posicion = 0;
	for (Object *item : items) {
		fprintf (file->c (), "%s", item->__repr__ ());
		if (posicion < (items.size () - 1))
			fprintf (file->c (), "%s", sep->__repr__ ());
		
		posicion ++;
	}
	fprintf (file->c (), "%s", end->__repr__ ());

	if (flush->c ()) {
		fflush (file->c ());
	}

	return None;
}

#endif
